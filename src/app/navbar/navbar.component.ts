import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {
  
  loggingIn: boolean;
  constructor(private router: Router) { }

  ngOnInit() {
    this.checkRoute(window.location.pathname)
  }

  checkRoute(router: string){
    console.log(router);
    if(router == '/login'){
      this.loggingIn = true;
    } else {
      this.loggingIn = false;
    }
  }

  title: string = "Cebu Specialists Ambulatory Care Center";
}
